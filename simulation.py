import numpy
import matplotlib.pyplot as plt
import os
import math

N = int (input ('Enter the number of molecules: '))#1
L = int (input ('Enter the box length: '))#2
n_steps = int (input ('Enter the number of steps: '))#3

# 1:To specify the number of molecules as needed,
# 2:To specify the box length as needed, 
# 3:To specify number of steps in simulation

pos = numpy.zeros((N,2))
numpy.random.seed(123)

def rand_position(position):
    return numpy.random.rand(1,2)*L

def pbc(pos_i):
    for i,p in enumerate(pos_i):
        if p>L:
            pos_i[i]=p-L
        if p<0.:
            pos_i[i]=p+L
    return pos_i

def rand_perturb(position,d=0.5):
    p = numpy.copy(position)
    dx = d*(numpy.random.rand()*2.0 - 1.0)
    dy = d*(numpy.random.rand()*2.0 - 1.0)
    p[0]+=dx
    p[1]+=dy
    return pbc(p)

# Defining distance as d.
def distance(pos_i,pos_j):
    d = pos_i - pos_j
    d = numpy.linalg.norm(d)
    return d

def overlap(i,pos_i):
    for j in xrange(N):
        d = distance(pos_i,pos[j])
        if d<1.0:
            if i !=j:
                return True
    return False

def move(i,move_type=rand_perturb):
    temp=move_type(pos[i])
    if not overlap(i,temp):
        pos[i] = temp

# This function gives a graph when each molecule is moved, which gives steps from 1 to N.
def initialize():
    for i in range(N):
        while overlap(i,pos[i]):
            move(i,rand_position)

# This code will name the current simulation and make a folder with that name
run_name="molecules_{}_length_{}_steps_{}".format(N,L,n_steps)
if not os.path.exists(run_name):
	os.mkdir(run_name)

# Functions that create and define the variables of the plots.
def draw(prefix='init',name=0):
    ax = plt.gca()
    for p in pos:
        a = plt.Circle((p[0],p[1]),0.5,color='b')
        ax.add_artist(a)
    ax.set_xlim((0,L))
    ax.set_ylim((0,L))
    plt.savefig('{}/{}.png'.format(run_name,name))
    plt.clf()

def run(n_steps):
    for s in xrange(n_steps):
        i = numpy.random.random_integers(0,N-1)
        move(i)

#This code will start the simulation and generate graphs
initialize()
draw('final',0)
run(n_steps)
plt.clf()
draw('n_steps',1)

#This code will generate a density plot for the number of molecules and box length specified
n = numpy.arange(N)

density = []
for i in range(N):
        Density = (((n+1)*math.pi*0.5*0.5)/(L*L))*100
        density = Density
run_name= "Density_plots"
print Density, N
plt.plot(density,n)
plt.title('Area Density (%)')
plt.xlabel('Area Density')
plt.ylabel('Number of molecules')
plt.savefig('{}/{}_molecules_Density_Plot.png'.format(run_name,N))



# Function to print a line that shows the program is complete, and where to find the plots as stated bellow.
print"\n --> Program complete, check directory:\n\n\t --> '%s' to locate Plot graphs\n\n" % os.getcwd()

