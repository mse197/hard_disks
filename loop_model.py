import numpy
import matplotlib.pyplot as plt
import os
import math
from random import randint	#Imports randint for Issue 18
import shutil

def rand_position(position):
    return numpy.random.rand(1,2)*L

def pbc(pos_i):
    for i,p in enumerate(pos_i):
        if p>L:
            pos_i[i]=p-L
        if p<0.:
            pos_i[i]=p+L
    return pos_i

def rand_perturb(position,d=0.5):
    p = numpy.copy(position)
    dx = d*(numpy.random.rand()*2.0 - 1.0)
    dy = d*(numpy.random.rand()*2.0 - 1.0)
    p[0]+=dx
    p[1]+=dy
    return pbc(p)

def calc_density(N,L):
    return N*math.pi*0.5*0.5/(L*L)

def calc_contact():
    contacts = 0
    for i,p in enumerate(pos):
        for j in range(i+1,N):
            d = distance(p,pos[j])
            if d<1.02:
                contacts+=1
    return contacts

def calc_pressure():
    nc = calc_contact()
    d = calc_density(N,L)
    g = nc/(N*d*2*math.pi*.02)
    return d*(1.+math.pi*d*g/2.)


# Defining distance as d.
def distance(pos_i,pos_j):
    d = pos_i - pos_j
    d = numpy.linalg.norm(d)
    return d

def overlap(i,pos_i):
    for j in xrange(N):
        d = distance(pos_i,pos[j])
        if d<1.0:
            if i !=j:
                return True
    return False

def move(i,move_type=rand_perturb):
    temp=move_type(pos[i])
    if not overlap(i,temp):
        pos[i] = temp
        return True
    return False

# This function gives a graph when each molecule is moved, which gives steps from 1 to N.
def initialize():
    for i in range(N):
        while overlap(i,pos[i]):
            move(i,rand_position)


# Functions that create and define the variables of the plots.
def draw(prefix='init',name=0):
    ax = plt.gca()
    for p in pos:
        a = plt.Circle((p[0],p[1]),0.5,color='b')
        ax.add_artist(a)
    ax.set_xlim((0,L))
    ax.set_ylim((0,L))
    plt.savefig('{}/{}.png'.format(run_name,name))
    plt.clf()

def run(n_steps):
    accepted = 0
    for s in xrange(n_steps):
        i = numpy.random.random_integers(0,N-1)
        if move(i):
            accepted+=1
            if accepted%100==0: #measures pressure after every 100 accepted moves
                #print "Working on step",s,"/",n_steps
                pressures.append(calc_pressure())

loop = int(input("Enter the number of runs: "))
for x in range(0,loop):
	N = int (input ('Enter the number of molecules: '))#1
	L = int (input ('Enter the box length: '))#2
	n_steps = int (input ('Enter the number of steps: '))#3

	# 1:To specify the number of molecules as needed,
	# 2:To specify the box length as needed, 
	# 3:To specify number of steps in simulation

	#Code in """.""" is to randomize initial positions (for Issue 18). If in use then delete line "numpy.random.seed(123)
	"""random_ask = raw_input("Do you want to randomize initial position of molecules? ")
	
	if random_ask in ['y', 'Y', 'yes', 'Yes']:
        	numpy.random.seed(randint(1,1000))"""
	pos = numpy.zeros((N,2))
	numpy.random.seed(123)
	pressures = []

	# This code will name the current simulation and make a folder with that name
	run_name="molecules_{}_length_{}_steps_{}".format(N,L,n_steps)
	if not os.path.exists(run_name):
        	os.mkdir(run_name)

	#Code will start simulation and generate plots
	initialize()
	draw('final',0)
	run(n_steps)
	draw('n_steps',1)
	plt.plot(pressures)
	plt.savefig("{}/pressure_plot.png".format(run_name))
	plt.clf()

	#Functions to print average pressure and standard deviation for every run
	pressures_avg = numpy.mean(pressures)
	pressures_std = numpy.std(pressures, ddof=1)
	print "Average Pressure = %r" % pressures_avg
	print "Standard Deviation = %r" % pressures_std

	#Code pipes all the pressures values, and average pressure, into a text file with its simulation directory
	step_output = open('{}/pressures.txt'.format(run_name), 'w')
	print >> step_output, pressures, pressures_avg

	#This code will generate a density plot for the number of molecules and box length specified
	n = numpy.arange(N)

	density = []
	for i in range(N):
        	Density = (((n+1)*math.pi*0.5*0.5)/(L*L))*100
        	density = Density

	print "Total Area Density Percent = %r" % Density[-1]
	plt.plot(density,n)
	plt.title('Area Density (%)')
	plt.xlabel('Area Density')
	plt.ylabel('Number of molecules')
	plt.savefig('{}/density_plot.png'.format(run_name))
	plt.clf()
	
	#This code will write the pressure and density values of the simulation into a txt file
	f = open('pressure_vs_density', 'a')
	f.write(str(pressures_avg))
	f.write('    ')
	f.write(str(Density[-1]))
	f.write("\n")
	f.close()

	# Function to print a line that shows the program is complete, and where to find the plots as stated bellow.
	print"\n --> Program complete, check directory:\n\n\t --> '%s' to locate Plot graphs\n\n" % os.getcwd()

#Defines two variables where the density and pressure values from our end-simulation file will be stored
density_total = []
avg_pressure = []

#Defines a function to get pressure valus from our file and store them in our variable
def get_pressure(file_in):
	file = open(file_in)
	for line in file:
		a = line.split()
		avg_pressure.append(float(a[0]))
	return avg_pressure

#Defines a function to get density values from our file and store them in our variable
def get_density(file_in):
	file = open(file_in)
	for line in file:
		a = line.split()
		density_total.append(float(a[1]))
	return density_total


#Will plot average pressure as a function of %density
get_pressure('pressure_vs_density')
get_density('pressure_vs_density')
plt.plot(density_total, avg_pressure)
plt.title('Pressure vs Density')
plt.xlabel('Area Density (%)')
plt.ylabel('Average Pressure')
plt.savefig('{}/pressure_vs_density_plot.png'.format(run_name))
plt.clf()


