import numpy
import matplotlib.pyplot as plt
import os
import math


N = int (input ('Enter the number of molecules: '))#1
L = int (input ('Enter the box length: '))#2
n = numpy.arange(N)

# 1:To specify the number of molecules as needed,
# 2:To specify the box length as needed,
# 3:Converting integer number into an array with length = N 
 
 
density = [] #creating an empty vector
for i in range(N): #looping to calculate the area density as N molecules are been added 
	Density = (((n+1)*math.pi*0.5*0.5)/(L*L))*100
	density = Density # Attribute the calculated values of density into the previous vector.


runname2= "Density_plots" #Defining name of directory that will be created to store the Area density plots

if not os.path.exists(runname2): #conditional that will allow the code to create the directory if it doesn't exist yet
        os.mkdir(runname2)
print Density, N
plt.plot(density,n)
plt.title('Area Density (%)')
plt.xlabel('Area Density')
plt.ylabel('Number of molecules')
plt.savefig('{}/{}_molecules.png'.format(runname2,N))

