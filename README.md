Use Monte Carlo method to measure how the pressure of a system of hard disks varies with density.

Monte Carlo methods  are a broad class of computational algorithms that rely on repeated random sampling to obtain numerical results, these methods are mainly used in three distinct problem classes: optimization, numerical integration, and generating draws from a probability distribution.

Reference: Wikipedia.

To make sure that the code will runs well the box lenght should be >= ((number of moluecules)/20) 
Exemple: If you have 400 molecules, your box lenght must be bigger or equal to 20.
Obs.1: if you try to runn 400 molecules, in a box with length = 20 it will take a really long time, we suggest that you always try to run simulations where the box lenght is >= ((number of molecules/20)+5
A few factora the are important to know before run the program are that: as higher your number of molecules/lenght of the box ratio is, faster the program will run, and higher the oportunitie for the molecules to move, and lower the pressue will be.

You also should know that the system do bigger changes as we use higer number of steps, giving more oportunities for the molecules to change the position, but it will take a longer time.

Obs.:  to run the program you should run "loop__model.py"
